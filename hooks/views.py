# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from tasks import sum, sentiment_analysis, adult_content_detection, drugs_content_detection, bullying_content_detection, violence_content_detection
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

@csrf_exempt
def example(request):
    if request.method == 'POST':
        if 'x' in request.POST and 'y' in request.POST:
           sum.apply_async(kwargs={'x': request.POST['x'], 'y': request.POST['y']})
           return HttpResponse()
    return HttpResponseNotFound()

@csrf_exempt
def sentiment(request):
    if request.method == 'POST':
        if 'commentIds' in request.POST and 'host' in request.POST and 'db' in request.POST and 'collection' in request.POST:
            sentiment_analysis.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId'], 'host': request.POST['host'], 'db': request.POST['db'], 'collection': request.POST['collection']})
            return HttpResponse()        
        elif 'commentIds' in request.POST:
            sentiment_analysis.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId']})
            return HttpResponse()
        else:
            return HttpResponseNotFound()
    return HttpResponseNotFound()

@csrf_exempt
def adult(request):
    if request.method == 'POST':
        if 'commentIds' in request.POST and 'host' in request.POST and 'db' in request.POST and 'collection' in request.POST:
            adult_content_detection.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId'], 'host': request.POST['host'], 'db': request.POST['db'], 'collection': request.POST['collection']})
            return HttpResponse()        
        elif 'commentIds' in request.POST:
            adult_content_detection.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId']})
            return HttpResponse()
        else:
            return HttpResponseNotFound()
    return HttpResponseNotFound()

@csrf_exempt
def drugs(request):
    if request.method == 'POST':
        if 'commentIds' in request.POST and 'host' in request.POST and 'db' in request.POST and 'collection' in request.POST:
            drugs_content_detection.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId'], 'host': request.POST['host'], 'db': request.POST['db'], 'collection': request.POST['collection']})
            return HttpResponse()        
        elif 'commentIds' in request.POST:
            drugs_content_detection.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId']})
            return HttpResponse()
        else:
            return HttpResponseNotFound()
    return HttpResponseNotFound()

@csrf_exempt
def bullying(request):
    if request.method == 'POST':
        if 'commentIds' in request.POST and 'host' in request.POST and 'db' in request.POST and 'collection' in request.POST:
            bullying_content_detection.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId'], 'host': request.POST['host'], 'db': request.POST['db'], 'collection': request.POST['collection']})
            return HttpResponse()        
        elif 'commentIds' in request.POST:
            bullying_content_detection.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId']})
            return HttpResponse()
        else:
            return HttpResponseNotFound()
    return HttpResponseNotFound()

@csrf_exempt
def violence(request):
    if request.method == 'POST':
        if 'commentIds' in request.POST and 'host' in request.POST and 'db' in request.POST and 'collection' in request.POST:
            violence_content_detection.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId'], 'host': request.POST['host'], 'db': request.POST['db'], 'collection': request.POST['collection']})
            return HttpResponse()        
        elif 'commentIds' in request.POST:
            violence_content_detection.apply_async(kwargs={'object_id': request.POST['commentIds'], 'childId':request.POST['childId']})
            return HttpResponse()
        else:
            return HttpResponseNotFound()
    return HttpResponseNotFound()