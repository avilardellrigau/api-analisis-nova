from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^example/$', views.example, name='example'),
    url(r'^sentiment/$', views.sentiment, name='sentiment'),
    url(r'^adult/$', views.adult, name='adult'),
    url(r'^drugs/$', views.drugs, name='drugs'),
    url(r'^bullying/$', views.bullying, name='bullying'),
    url(r'^violence/$', views.violence, name='violence'),
]
