from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from libs.TextAnalyzer import TextAnalyzer
from libs.TextAnalyzer import stem_Spanish_Phrase
from django.conf import settings
from pymongo import MongoClient
from bson.objectid import ObjectId
from sklearn.externals import joblib
from datetime import datetime
from nltk.tokenize import word_tokenize
import nltk
import Stemmer 

def match(l, m):
    c = []
    # print(any([x in m for x in l]))
    a = [x in m for x in l] # retorna true o false
    print(a)
    b = [i for i, x in enumerate(a) if x] # retorna l'index de les paraules que fan match
    print(b)
    #c = [b[i] for i in b]
    for x in b:
        c.append(l[x])
    return c

def match2(message): 
    pipe = joblib.load('libs/drugs_pipeline.pkl')    
    comment = nltk.tokenize.word_tokenize(message)
    c = []
    for x in comment: 
        if int(pipe.predict([x])[0]) == 1 :
            c.append(x)   
    return c


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'celery_webhooks.settings')

app = Celery('celery_webhooks')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.task(bind=True)
def sum(self, x, y):
    return x + y

@app.task(bind=True)
def sentiment_analysis(self, object_id, childId, host='j1.jimcrickapp.com', db='bulltec', collection='comments'):
    text_analyzer = TextAnalyzer(sms_dictionary_path = "libs/dict/sms_dict.json", emoticon_dictionary_path = "libs/dict/emo_dict.json")
    client = MongoClient(host, 27017, authSource='bulltec', authMechanism='SCRAM-SHA-1', username='bulltec', password='NG7bulltec')
    print(client)    
    mongo_collection = client.comments
    object_id = str(object_id)
    for id in object_id.split('_'):
        try:
            comment = mongo_collection.find_one({'_id': ObjectId(id)})       
            sent = text_analyzer.SO_calculate(comment['message'], 'senti', 'es')            
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'analysis_results.sentiment.result': sent, 'analysis_results.sentiment.status': 'FINISHED', 'analysis_results.sentiment.finished_at': datetime.now()}}, upsert=True)
        except Exception as e:
            print e  
            #mongo_db.children.update_one({'_id': ObjectId(childId)}, {'$set': {"results.sentiment.obsolete":True}})

    return

@app.task(bind=True)
def adult_content_detection(self, object_id, childId, host='j1.jimcrickapp.com', db='bulltec', collection='comments'):

    mongo_client = MongoClient(host, 27017)
    mongo_db = mongo_client.bulltec
    mongo_db.authenticate('bulltec', 'NG7bulltec')
    mongo_collection = mongo_db.comments
    object_id = str(object_id)

    pipe = joblib.load('libs/adult_pipeline.pkl')

    for id in object_id.split('_'):
        try:
            comment = mongo_collection.find_one({'_id': ObjectId(id)})
            adult = int(pipe.predict([comment['message']])[0])
            c = match2(comment['message'])
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'adult.words': c }}, upsert=True) 
            if(adult > 0):
              mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'results.adult': 1 }}, upsert=True) 
            else :
              mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'results.adult': 0 }}, upsert=True) 
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'analysis_results.adult.result': adult, 'analysis_results.adult.status': 'FINISHED', 'analysis_results.adult.finished_at': datetime.now() }}, upsert=True)
        except Exception as e:
            print e
    #if mongo_collection.find({"target.$id":ObjectId(childId), "analysis_results.adult.status":"IN_PROGRESS"}).count() == 0:
    #    mongo_db.children.update_one({'_id': ObjectId(childId)}, {'$set': {"results.adult.obsolete":False}})
    mongo_db.children.update_one({'_id': ObjectId(childId)}, {'$set': {"results.adult.obsolete":True}})

    return

@app.task(bind=True)
def drugs_content_detection(self, object_id, childId, host='j1.jimcrickapp.com', db='bulltec', collection='comments'):

    mongo_client = MongoClient(host, 27017)
    mongo_db = mongo_client.bulltec
    mongo_db.authenticate('bulltec', 'NG7bulltec')
    mongo_collection = mongo_db.comments
    object_id = str(object_id)

    pipe = joblib.load('libs/drugs_pipeline.pkl')

    for id in object_id.split('_'):
        try:
            comment = mongo_collection.find_one({'_id': ObjectId(id)})
            drugs = int(pipe.predict([comment['message']])[0])
            c = match2(comment['message'])             
            mongo_collection.update_one({'_id': ObjectId(childId)}, {'$set': {"drugs.word": c}}, upsert=True)  
            if (drugs > 0):
                mongo_collection.update_one({'_id': ObjectId(childId)}, {'$set': {"results.drugs": 1}}, upsert=True)
            else:
                mongo_collection.update_one({'_id': ObjectId(childId)}, {'$set': {"results.drugs": 0}}, upsert=True)
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'analysis_results.drugs.result': drugs, 'analysis_results.drugs.status': 'FINISHED', 'analysis_results.drugs.finished_at': datetime.now() }}, upsert=True)
        except Exception as e:
            print e
    #if mongo_collection.find({"target.$id":ObjectId(childId), "analysis_results.drugs.status":"IN_PROGRESS"}).count() == 0:
    #mongo_db.children.update_one({'_id': ObjectId(childId)}, {'$set': {"results.drugs.obsolete":False}})
    
    mongo_db.children.update_one({'_id': ObjectId(childId)}, {'$set': {"results.drugs.obsolete":True}})

    return


@app.task(bind=True)
def bullying_content_detection(self, object_id, childId, host='j1.jimcrickapp.com', db='bulltec', collection='comments'):

    bullKeywords = open("libs/bullyingKeywords.txt").read().decode("utf-8").split(" ")
    text_analyzer = TextAnalyzer(sms_dictionary_path = "libs/dict/sms_dict.json", emoticon_dictionary_path = "libs/dict/emo_dict.json")
    mongo_client = MongoClient(host, 27017)
    mongo_db = mongo_client.bulltec
    mongo_db.authenticate('bulltec', 'NG7bulltec')
    mongo_collection = mongo_db.comments
    object_id = str(object_id)
    for id in object_id.split('_'):
        try:
            comment = mongo_collection.find_one({'_id': ObjectId(id)})
            sent = text_analyzer.SO_calculate(comment['message'], 'senti', 'es')
            bull = any([x in comment['message'].lower() for x in bullKeywords])
            bullw = match(nltk.tokenize.word_tokenize(comment['message']), bullKeywords)
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'bullying.words': bullw}}, upsert=True) 
            result = 1 if (bull and sent < 0) else 0 #Marcamos un mensaje como bullying si es negativo y contiene insultos/palabrotas
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'results.bullying': result}}, upsert=True)         
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'analysis_results.bullying.result': result, 'analysis_results.bullying.status': 'FINISHED', 'analysis_results.bullying.finished_at': datetime.now() }}, upsert=True)
        except Exception as e:
            print e

    #if mongo_collection.find({"target.$id":ObjectId(childId), "analysis_results.bullying.status":"IN_PROGRESS"}).count() == 0:
    #    mongo_db.children.update_one({'_id': ObjectId(childId)}, {'$set': {"results.bullying.obsolete":False}})
    mongo_db.children.update_one({'_id': ObjectId(childId)}, {'$set': {"results.bullying.obsolete":True}})

    return

@app.task(bind=True)
def violence_content_detection(self, object_id, childId, host='j1.jimcrickapp.com', db='bulltec', collection='comments'):

    violenceKeywords = open("libs/violenceKeywords.txt").read().decode("utf-8").split(" ")
    stemmer = Stemmer.Stemmer("spanish")
    violenceKeywords = stemmer.stemWords(violenceKeywords)
    TextAnalyzer(sms_dictionary_path = "libs/dict/sms_dict.json", emoticon_dictionary_path = "libs/dict/emo_dict.json")
    mongo_client = MongoClient(host, 27017)
    mongo_db = mongo_client.bulltec
    mongo_db.authenticate('bulltec', 'NG7bulltec')
    mongo_collection = mongo_db.comments
    object_id = str(object_id)

    #TODO:
    # TRY-CATCH
    # Marcar ninios como Obsolete...
    # Taoboada y demas todo en una carpeta
    # Manual de despliegue

    for id in object_id.split('_'):
        try:
            comment = mongo_collection.find_one({'_id': ObjectId(id)})            
            stemmedComment = stem_Spanish_Phrase(comment['message'].lower())
            violence = any([x in stemmedComment for x in violenceKeywords])
            v = match(nltk.tokenize.word_tokenize(comment['message']), violenceKeywords)
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'violence.words': v }}, upsert=True)
            result = 1 if violence else 0
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'results.violence': result}}, upsert=True)
            mongo_collection.update_one({'_id': ObjectId(id)}, {'$set': { 'analysis_results.violence.result': int(result), 'analysis_results.violence.status': 'FINISHED', 'analysis_results.violence.finished_at': datetime.now() }}, upsert=True)
        
        except Exception as e:
            print e

    #if mongo_collection.find({"target.$id":ObjectId(childId), "analysis_results.violence.status":"IN_PROGRESS"}).count() == 0:
    #    mongo_db.children.update_one({'_id': ObjectId(childId)}, {'$set': {"results.violence.obsolete":False}})
    mongo_db.children.update_one({'_id': ObjectId(childId)}, {'$set': {"results.violence.obsolete":True}})
    
    return
