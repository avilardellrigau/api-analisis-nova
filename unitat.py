# coding=utf-8
'''Definim codificacio UTF-8'''
from __future__ import absolute_import, unicode_literals
import json
from langdetect import detect
import nltk
import nltk.corpus

class Analisi(object):
    '''Defincio de la classe Analisi'''

    def __init__(self, language):
        self.language = language

    #Funcio que comprova si la paraula fa match amb els diccionaris
    def match(self, character_a, character_b):
        '''Definim un array de paraules buit'''
        array_words = []
        words_a = [x in character_b for x in character_a]
        words_b = [i for i, x in enumerate(words_a) if x] # retorna id de les paraules que fan match
        for word in words_b:
            array_words.append(character_a[word])
        return array_words

    #Funcio per comprovar si tenim paraules relacionades amb bullying
    def get_bullying(self, comment):
        '''Obtenim el llenguatge del comment'''
        language = detect(comment)
        array_words = []
        if language == 'es':
            tokens = nltk.tokenize.word_tokenize(comment, language='spanish')
            dictionari = 'resources/txt/spanish/bullying_keywords_spanish.txt'
            keywords = open(dictionari).read().decode("utf-8").split(" ")
            array_words = self.match(tokens, keywords)
        return array_words

    #Funcio per comprovar si tenim paraules relacionades amb violence
    def get_violence(self, comment):
        '''Obtenim el llenguatge del comment'''
        array_words = []
        tokens = nltk.tokenize.word_tokenize(comment, language='spanish')
        dictionari = 'resources/txt/spanish/violence_keywords_spanish.txt'
        keywords = open(dictionari).read().decode("utf-8").split(" ")
        array_words = self.match(tokens, keywords)
        return array_words

    #Funcio per comprovar si tenim paraules relacionades amb drugs
    def get_drugs(self, comment):
        '''Obtenim el llenguatge del comment'''
        array_words = []
        tokens = nltk.tokenize.word_tokenize(comment, language='spanish')
        dictionari = 'resources/txt/spanish/drugs_keywords_spanish.txt'
        keywords = open(dictionari).read().decode("utf-8").split(" ")
        array_words = self.match(tokens, keywords)
        return array_words

    #Funció per comprovar si tenim paraules relacionades amb contingut per adults
    def get_adult(self, comment):
        '''Obtenim el llenguatge del comment'''
        language = detect(comment)
        array_words = []
        if language == 'es':
            tokens = nltk.tokenize.word_tokenize(comment, language='spanish')
            dictionari = 'resources/txt/spanish/adult_keywords_spanish.txt'
            keywords = open(dictionari).read().decode("utf-8").split(" ")
            array_words = self.match(tokens, keywords)
        elif language == 'en':
            tokens = nltk.tokenize.word_tokenize(comment, language='english')
            dictionari = 'resources/txt/english/adult_keywords_english.txt'
            keywords = open(dictionari).read().decode("utf-8").split(" ")
            array_words = self.match(tokens, keywords)
        return array_words

    def get_query(self, arrayparameters):
        '''Obtenim l'array de parametres'''
        index = 1
        while index == 1:
            query = {
                "$set": {
                    "violence_words": arrayparameters[index-1],
                    "violence_results": arrayparameters[index],
                    "violence_danger_value": arrayparameters[index+1],
                    "bullying_words": arrayparameters[index+2],
                    "bullying_results": arrayparameters[index+3],
                    "bullying_danger_value": arrayparameters[index+4],
                    "adult_words": arrayparameters[index+5],
                    "adult_results": arrayparameters[index+6],
                    "adult_danger_value": arrayparameters[index+7],
                    "drugs_words": arrayparameters[index+8],
                    "drugs_results": arrayparameters[index+9],
                    "drugs_danger_value": arrayparameters[index+10],
                    "nn": arrayparameters[index+11],
                    "adj": arrayparameters[index+12],
                    "status": True
                }
            }
            index = index + 1
        return query

    #Get nouns from a comment
    def get_nouns(self, comment):
        '''Obtenim el llenguatge del comment'''
        language = detect(comment)
        nouns = []
        if language == 'es':
            tokens = nltk.tokenize.word_tokenize(comment, language='spanish')
            dictionari = "resources/txt/lexic/noms.txt"
            keywords = open(dictionari).read().decode("utf-8").split(" ")
            nouns = self.match(tokens, keywords)
        elif language == 'en':
            tokens = nltk.tokenize.word_tokenize(comment, language='english')
            dictionari = "resources/txt/lexic/english-nouns.txt"
            keywords = open(dictionari).read().decode("utf-8").split(" ")
            nouns = self.match(tokens, keywords)
        return nouns

    #Get adjectives from a comment
    def get_adjectives(self, comment):
        '''Obtenim el llenguatge del comment'''
        language = detect(comment)
        adjectives = []
        if language == 'es':
            tokens = nltk.tokenize.word_tokenize(comment, language='spanish')
            dictionari = "resources/txt/lexic/adj.txt"
            keywords = open(dictionari).read().decode("utf-8").split(" ")
            adjectives = self.match(tokens, keywords)
        elif language == 'en':
            tokens = nltk.tokenize.word_tokenize(comment, language='english')
            dictionari = "resources/txt/lexic/english-adjectives.txt"
            keywords = open(dictionari).read().decode("utf-8").split(" ")
            adjectives = self.match(tokens, keywords)
        return adjectives

    #Método para obtener la valoración de los comentarios con contenido de las distintas dimensiones
    def comparative_comments(self, comment, lang, type_dim):
        '''Comparative comments'''
        fitxer = " "
        if type_dim == "bullying":
            if lang == 'es':
                fitxer = "resources/json/bullying_es.json"
            elif lang == 'ca':
                fitxer = "resources/json/bullying_ca.json"
        elif type_dim == "adult":
            if lang == 'es':
                fitxer = "resources/json/adult_es.json"
            elif lang == 'ca':
                fitxer = "resources/json/adult_ca.json"
        elif type_dim == "violence":
            if lang == 'es':
                fitxer = "resources/json/violence_es.json"
            elif lang == 'ca':
                fitxer = "resources/json/violence_ca.json"
        elif type_dim == "drugs":
            if lang == 'es':
                fitxer = "resources/json/drugs_es.json"
            elif lang == 'ca':
                fitxer = "resources/json/drugs_ca.json"
        perillositat = 0
        if comment.__len__() > 0:
            data = []
            value = []
            with open(fitxer) as data_file:
                data = json.load(data_file)
                items_array = data["values"]
                for items in items_array:
                    for word in comment:
                        if items["word"].__contains__(word):
                            value.append(items["value"])
                perillositat = max(value)
            return perillositat
        else:
            print "No hay palabras relacionadas con " + str(type_dim)
            perillositat = 0
            return perillositat
