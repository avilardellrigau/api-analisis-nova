# coding=utf-8
'''Definim codificacio UTF-8'''
import time
from langdetect import detect
from bd import BD
from unitat import Analisi

#Crida als constructors de les classes BD i Analisi#
BD = BD()
ANALISI = Analisi(object)
#Bucle infinit que busca els comentaris i classifica les paraules segons la seva dimensio
#Tambe obtenim els adjectius i noms que en formen part
while True:
    #Pintem la data d'inici del programa
    print time.strftime('%Y-%m-%d %H:%M:%S') + " - Inici"
    CON = BD.getConnection()
    MONGO_DB = CON.bulltec
    MONGO_COLLECTION = MONGO_DB.comments
    COMMENTS = MONGO_COLLECTION.find({"status": {"$ne": True}})
    #Iterem els comentaris
    for x in COMMENTS:
        NOUNS = ANALISI.get_nouns(x.get(u'message'))
        ADJECTIVES = ANALISI.get_adjectives(x.get(u'message'))
        #Pintem els noms i adjectius
        LANG = detect(x.get(u'message'))
        #Comprovem si hi ha paraules relacionades amb bullying
        BULLYING_WORDS = ANALISI.get_bullying(x.get(u'message'))
        BULLYING_SENTIMENT = ANALISI.comparative_comments(BULLYING_WORDS, LANG, "bullying")
        if BULLYING_WORDS:
            BULLYING_RESULTS = True
        else:
            BULLYING_RESULTS = False
        #Comprovem si hi ha paraules relacionades amb violencia
        VIOLENCE_WORDS = ANALISI.get_violence(x.get(u'message'))
        VIOLENCE_SENTIMENT = ANALISI.comparative_comments(VIOLENCE_WORDS, LANG, "violence")
        if VIOLENCE_WORDS:
            VIOLENCE_RESULTS = True
        else:
            VIOLENCE_RESULTS = False
        #Comprovem si hi ha paraules relacionades amb sexe
        ADULT_WORDS = ANALISI.get_adult(x.get(u'message'))
        ADULT_SENTIMENT = ANALISI.comparative_comments(ADULT_WORDS, LANG, "adult")
        if ADULT_WORDS:
            ADULT_RESULTS = True
        else:
            ADULT_RESULTS = False
        #Comprovem si hi ha paraules relacionades amb drogues
        DRUGS_WORDS = ANALISI.get_drugs(x.get(u'message'))
        DRUGS_SENTIMENT = ANALISI.comparative_comments(DRUGS_WORDS, LANG, "drugs")
        if DRUGS_WORDS:
            DRUGS_RESULTS = True
        else:
            DRUGS_RESULTS = False
        #Obtenir el JSON de la query per afegir l'analisi a la MongoDB
        ARRAY_WORDS = []
        ARRAY_WORDS.append(VIOLENCE_WORDS)
        ARRAY_WORDS.append(VIOLENCE_RESULTS)
        ARRAY_WORDS.append(VIOLENCE_SENTIMENT)
        ARRAY_WORDS.append(BULLYING_WORDS)
        ARRAY_WORDS.append(BULLYING_RESULTS)
        ARRAY_WORDS.append(BULLYING_SENTIMENT)
        ARRAY_WORDS.append(ADULT_WORDS)
        ARRAY_WORDS.append(ADULT_RESULTS)
        ARRAY_WORDS.append(ADULT_SENTIMENT)
        ARRAY_WORDS.append(DRUGS_WORDS)
        ARRAY_WORDS.append(DRUGS_RESULTS)
        ARRAY_WORDS.append(DRUGS_SENTIMENT)
        ARRAY_WORDS.append(NOUNS)
        ARRAY_WORDS.append(ADJECTIVES)
        print ARRAY_WORDS
        QUERY = ANALISI.get_query(ARRAY_WORDS)
        print QUERY
        #Afegir o modificar els camps a la base de dades en funcio de si existeixen o no
        MONGO_COLLECTION.update_one({"_id": x.get(u'_id')}, QUERY, upsert=True)
        #Pintem la data de finalitzacio
        print time.strftime('%Y-%m-%d %H:%M:%S') + " - Final"
        CON.close()
        #El programa fa un sleep de 120 segons
        time.sleep(20)
