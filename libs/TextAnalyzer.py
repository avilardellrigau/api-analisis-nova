# pylint: skip-file
# -*- coding: utf-8 -*
'''Definicion de importaciones'''
from __future__ import division
import json
import sys
from os.path import join
import os
import re
import Queue
import string
import subprocess
import shlex
import shutil
from uuid import uuid1
import distance
from enchant.checker import SpellChecker
from enchant.tokenize import EmailFilter, URLFilter
from twitter_text import Extractor
import Stemmer
from unidecode import unidecode

def stem_Spanish_Phrase(phrase):
    '''Metodo para limpiar los espacios de un array de palabras de una frase'''
    stemmed = ""
    stemmer = Stemmer.Stemmer("spanish")
    for word in stemmer.stemWords(phrase.split(" ")):
        stemmed += word+" "
    return stemmed

class TextAnalyzer(object):
    '''Clase TextAnalyzer'''

    SPANISH_PREPROCESS_DIR = join(os.getcwd(), "libs/SO_CAL/Spanish_preprocessor/")
    SO_CAL_DIR = join(os.getcwd(), "libs/SO_CAL/SO-CAL_V1.11/")
    LANG = "es"

    def __init__(self, sms_dictionary_path, emoticon_dictionary_path):
        '''Constructor de la clase TextAnalyzer'''
        self.sms_dictionary = json.loads(open(self.SO_CAL_DIR+"dict/sms_dict.json").read())
        self.emoticon_dictionary = json.loads(open(self.SO_CAL_DIR+"dict/emo_dict.json").read())

    def spellCorrect(self, text, entities=[], lang='es'):
        """La función corrige un texto y lo devuelve limpio de elementos
        que se encuentran típicamente en textos de internet.
        :param text: El texto a corregir.
        :type text: str.
        :param entities: Entidades que el corrector debe ignorar en el texto.
        :type entities: list.
        :returns: str -- La frase corregida.
        """
        print text
        print lang

        reload(sys)
        sys.setdefaultencoding('utf8')
        enchantDict = {'es': 'es_ES', 'en':'en_US', 'fr':'fr_FR', 'de':'de_DE', 'ru':'ru_RU'};
        person = {'es': 'persona', 'en':'person', 'fr':'personne', 'de':'person', 'ru':'человек'};

        chkr = SpellChecker(enchantDict[lang], filters=[EmailFilter, URLFilter])
        text = text.replace("RT", "")
        extractor = Extractor(text)
        mentioned_screen_names = extractor.extract_mentioned_screen_names()
        urls = extractor.extract_urls()
        hashtags = extractor.extract_hashtags()
        for name in mentioned_screen_names:
            text = text.replace("@"+name, person[lang])
        for url in urls:
            text = text.replace(url, "")
        for hashtag in hashtags:
            sub = re.compile(re.escape("#"+hashtag), re.IGNORECASE)
            text = sub.sub("", text)
        text = text.replace("...", "")
        text = text.replace("http://", "")
        text = text.replace("http:/", "")
        text = text.replace("/", "")
        text = text.replace(",", "")
        text = text.replace("%", "")
        text = re.sub(r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))''', '', text, flags=re.MULTILINE)
        text = re.sub(r'([,/\\]+)(?![0-9])', r'\1 ', text)
        text = re.sub(r'([,\.]+)(?![0-9])', r'\1 ', text)
        text = text.replace(";", "")
        chkr.set_text(text)
        for err in chkr:
            corrected = False
            if err.word not in entities:
                if err.word not in mentioned_screen_names:
                    if corrected is False:
                        corrections = err.suggest()
                        queue = Queue.PriorityQueue()
                        for correction in corrections[:10]:
                            correction = (distance.levenshtein(correction, err.word), correction)
                            without_accents = unidecode(correction[1])
                            if err.word == without_accents:
                                correction = (0L, correction[1])
                            queue.put(correction)
                        if not queue.empty():
                            err.replace(queue.get()[1])
        return chkr.get_text()

    def emoticonCorrect(self, text):
        '''Metodo que devuelve true o false dependiendo de si el emoticono es correcto o no'''
        dic = self.emoticon_dictionary
        self.pattern = re.compile('|'.join(re.escape(em) for emo in dic for em in emo.split('|')))
        return self.pattern.sub(self.emoText, text)

    def spanish_preprocess(self, input_dir, output_dir):
        '''Metodo que ejecuta el spanish_preprocess'''
        os.chdir(self.SPANISH_PREPROCESS_DIR)
        subprocess.call(["sh Spanish_preprocess.sh " + input_dir + " " + output_dir], shell=True)
        os.system("rm -R "+input_dir)

    def utf8tolatin1(self, in_dir, out_dir, file):
        '''Metodo para codificar en UTF8'''
        os.chdir(self.SPANISH_PREPROCESS_DIR + in_dir)
        cod = "iconv -c -f UTF-8 -t LATIN1 "
        codi = '"/latin_"'
        subprocess.call([cod + file + " > " + self.SO_CAL_DIR + out_dir + codi +file], shell=True)
        os.chdir(self.SPANISH_PREPROCESS_DIR)
        os.system("rm -R "+ in_dir)

    def so_run(self, input_dir, output_dir):
        '''Metodo para ejecutar SO_CAL'''
        os.chdir(self.SO_CAL_DIR)
        path = "python SO_Run.py -c Spa_SO_Calc.ini "
        subprocess.call([path + self.SO_CAL_DIR + input_dir + " " + output_dir], shell=True)
        os.chdir(self.SO_CAL_DIR + output_dir)
        file = open("output.txt", "r")
        for line in file.readlines():
            array = line.split("\t")
        file.close()
        return array[1]

    def SO_calculate(self, text, mode, lang, id=str(uuid1())):
        """ La función obtiene la polaridad del texto.
        :param text: El texto a determinar su polaridad.
        :type text: str.
        :param id: El ID único del texto a obtener su polaridad.
        :type id: str.
        :returns: float -- Valor de la polaridad del texto en una escala de -10 a 10
        """

        if lang != "es" or mode == 'senti':
            text = self.spellCorrect(unicode(text), lang=lang)
            return self.multi_calculate(text, lang)

        text = self.spellCorrect(text=self.emoticonCorrect(unicode(text)), lang='es')
        os.chdir(self.SPANISH_PREPROCESS_DIR)
        print os.getcwd()
        if os.path.exists("i_"+ id):
            shutil.rmtree("i_"+ id)
        os.mkdir("i_"+ id)
        if os.path.exists("o_"+ id):
            shutil.rmtree("o_"+ id)
        os.mkdir("o_"+ id)
        os.chdir(self.SO_CAL_DIR)
        if os.path.exists("i_"+ id):
            shutil.rmtree("i_"+ id)
        os.mkdir("i_"+ id)
        if os.path.exists("o_"+ id):
            shutil.rmtree("o_"+ id)
        os.mkdir("o_"+ id)
        os.chdir(self.SPANISH_PREPROCESS_DIR+ "i_"+ id)
        file = open(id+".txt", "w")
        file.write(text.encode('utf-8'))
        file.close()
        self.spanish_preprocess("i_"+ id, "o_"+ id)
        self.utf8tolatin1("o_"+ id, "i_"+id, id+".txt")
        sentiment = self.so_run("i_"+id, "o_"+id)
        os.system("rm -R "+self.SO_CAL_DIR+ "i_"+ id)
        os.system("rm -R "+self.SO_CAL_DIR+ "o_"+ id)
        print "SENTIMENT: "+ str(sentiment)
        return sentiment

    def multi_calculate(self, sentiString, lang):
        '''Metodo para calcular el sentiment_analysis'''
        #Preprocesamiento
        sentiString = re.sub(' +', ' ', sentiString)
        reload(sys)
        sys.setdefaultencoding('utf8')
        if lang != "ru":
            sentiString = filter(lambda x: x in string.printable, sentiString)
        #open a subprocess to get the command line string into the correct args list format
        sen = "java -jar /home/bulltec/celery_webhooks/libs/sentimentAnalysis/sentimentAnalysis.jar"
        jar = sen + "stdin sentidata /home/bulltec/celery_webhooks/libs/sentimentAnalysis/"
        pipe = subprocess.PIPE
        process = subprocess.Popen(shlex.split(jar+lang+"/"), stdin=pipe, stdout=pipe, stderr=pipe)
        #communicate via stdin the string to be rated. Note that all spaces are replaced with +
        stdout_text, stderr_text = process.communicate(sentiString.replace(" ", "+"))
        #remove the tab spacing between the positive and negative ratings. e.g. 1    -5 -> 1-5
        stdout_text = stdout_text.rstrip().replace("\t", "")
        sentTuple = (int(stdout_text[0]), -1*int(stdout_text[-1]))
        if abs(sentTuple[0]) <= 1 and abs(sentTuple[1]) <= 1:
            return 0
        if abs(sentTuple[0]) > abs(sentTuple[1]):
            print "Sentistr sentiment: "+str(2*sentTuple[0])+" for: "+sentiString
            return 2*sentTuple[0]
        else:
            print "Sentistr sentiment: "+str(2*sentTuple[1])+" for: "+sentiString
            return 2*sentTuple[1]

    def emoText(self, emoMatch):
        '''Metodo para formatear el diccionario de emoticonos'''
        if emoMatch.group(0) != '':
            return '{0}'.format(self.emoticon_dictionary[emoMatch.group(0)]['acepcion'])
