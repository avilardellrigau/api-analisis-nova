################################################################################################ 
# by Caroline Anthony 
# June 23, 2005 
# sentence_boundary.pl 
# This file looks for sentence boundaries and adds a newline after each
#
# Modified May 27th, 2008-- Kimberly Voll
# Added foreach loop to split based on newline and separate print out each sentence.  This
# removed the problem of the leading space.
#
# Modified June 25th, 2008-- Kimberly Voll
# Corrected bug regarding extra spaces surrounding abbreviations/titles.
##############################################################################################

use strict;

   my $line;
   while ($line = <>)
   {
	chomp($line);
	$line =~ s/(.+?[.!?][")]?)(?=(?:\s+["(]?[A-Z]|\s*$))/$1\n/gx;
   	
	#Look for abbreviations that were mistakenly put on a new line
        $line =~ s/^(Dr|Mr|Ms|Mrs|Miss|Sir|Hon|Jr|Sr|Cpl|Capt|Comdr|Sgt|Ltd|Corp|Inc|Ave|St|Blvd|Rd|dept|dist|div|est|etc|(A\.M)|(P \. M)|(U \. S)|Jan|Feb|Mar|Apr|Jun|Jul|Aug|Sept|Oct|Nov|Dec)\s+?\.\s+?\n?/$1\. /g;
        $line =~s/\s+(Dr|Mr|Ms|Mrs|Miss|Sir|Hon|Jr|Sr|Cpl|Capt|Comdr|Sgt|Ltd|Corp|Inc|Ave|St|Blvd|Rd|dept|dist|div|est|etc|(A \.M)|(P\. M)|(U \. S)|Jan|Feb|Mar|Apr|Jun|Jul|Aug|Sept|Oct|Nov|Dec)\s+?\.\s+?\n?/ $1\. /g;

   my @sentences= split(/\n/, $line);

   foreach (@sentences)
   {
	$_ =~ s/^\s+//;
	print $_ . "\n";

   }

   }


############################################################
# Here is a breakdown of the regular expression
#/(
#.+?	#match (non-greedy) anything...
#[.!?]	#followed by any one of .!?
#[")]?	#and optionally " or )   
#)
#(?=	#with lookahead that is followed by ...
#(?:	#either...
#\s+	#some whitespace...
#["(]?	#maybe a " or ( ...
#[A-Z]	#and capital letter
#|	#or...
#\s*$	#optional whitespace, followed by end of string
#)
#)
#/gx
#;
###############################################################
