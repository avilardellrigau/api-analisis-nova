# mget_stats.py by Julian Brooke (January 2009)
# This script is used in conjunction with get_stats.py to get the combined stats
# for the output of SO_Mrun.py.
# Usage:
# mget_stats.py *SO_Mrun.py output dir*
# the stats file (stats.txt) will be created in root output directory

import sys, os
directory = sys.argv[1]
sub_dirs = os.listdir(directory)
categories = []
stats = []
sub_output = []
for sub_dir in sub_dirs:
    if os.path.isdir(directory + "/" + sub_dir):
        os.system("python get_stats.py " + directory +"/"+ sub_dir + "/richout.txt " + directory +"/"+ sub_dir + "/stats.txt") 
        f = open(directory +"/"+ sub_dir + "/stats.txt")
        sub_stats = f.read()
        sub_output.append(sub_dir +"\n" + "------\n" + sub_stats + "\n")
        lines = sub_stats.split("\n")
        i = 0
        pairs=[]
        while i < len(lines):
            if "\t" in lines[i]: 
                pairs.append(lines[i].split("\t"))
            i += 1
        if categories == []:
            for pair in pairs:
                if pair:
                    categories.append(pair[0])
                    stats.append(0)
        num_texts = int(pairs[0][1])
        stats[0] += num_texts
        num_words = int(pairs[1][1])
        stats[1] += num_words
        i = 2
        while "Intensification" not in pairs[i][0]:
            stats[i] += float(pairs[i][1])*num_words
            i += 1
        num_int = float(pairs[i+2][1])*num_texts
        stats[i] += float(pairs[i][1])* num_int
        stats[i+2] += num_int
        i += 1
        num_neg = float(pairs[i+2][1])*num_texts
        stats[i] += float(pairs[i][1])* num_neg
        stats[i+2] += num_neg
        i += 3
        while i < len(pairs):
            stats[i] += float(pairs[i][1])*num_texts
            i += 1
        f.close()
f = open(directory + "/" + "stats.txt", "w")
f.write("Totals\n------\n")
f.write(categories[0] + "\t" + str(stats[0]) + "\n")
f.write(categories[1] + "\t" + str(stats[1]) + "\n")
i = 2
while "Intensification" not in categories[i]:
    f.write(categories[i] + "\t" + str(stats[i]/stats[1]) +"\n")
    i += 1
f.write(categories[i] + "\t" + str(stats[i]/stats[i + 2]) +"\n")
i+= 1
f.write(categories[i] + "\t" + str(stats[i]/stats[i + 2]) +"\n")
i += 1
while i < len(categories):
    f.write(categories[i] + "\t" + str(stats[i]/stats[0]) +"\n")
    i += 1
for chunk in sub_output:
    f.write("\n" + chunk + "\n" )
f.close()
    


    
        
        
              
             
