import sys, os
f = open ("dicc.src", "r")
accents = {chr(237):"i", chr(243):"o", chr(250):"u", chr(233):"e", chr(225):"a",chr(241):"n"}
def has_accent(word):
    for accent in accents:
        if accent in word:
            return True
    return False

def remove_accents(word):
    for accent in accents:
        word = word.replace(accent, accents[accent])
    return word

lemma_dictionary_N = {}
lemma_dictionary_V = {}
lemma_dictionary_A = {}
for line in f:
    if line:
        words_and_tags = line.strip().split()
        token = words_and_tags[0]
        for i in range(1, len(words_and_tags), 2):
            if words_and_tags[i+1][0] == "N" and token not in lemma_dictionary_N:
                lemma_dictionary_N[token] = words_and_tags[i]
            elif words_and_tags[i+1][0] == "V" and token not in lemma_dictionary_V:
                lemma_dictionary_V[token] = [words_and_tags[i],words_and_tags[i+1][3]]
            elif words_and_tags[i+1][0] == "A" and token not in lemma_dictionary_A:
                lemma_dictionary_A[token] = words_and_tags[i]
            if has_accent(token):
                token = remove_accents(token)
                if words_and_tags[i+1][0] == "N" and token not in lemma_dictionary_N:
                    lemma_dictionary_N[token] = words_and_tags[i]
                elif words_and_tags[i+1][0] == "V" and token not in lemma_dictionary_V:
                    lemma_dictionary_V[token] = [words_and_tags[i],words_and_tags[i+1][3]]
                elif words_and_tags[i+1][0] == "A" and token not in lemma_dictionary_A:
                    lemma_dictionary_A[token] = words_and_tags[i]
                
f.close()
in_dir = sys.argv[1]
out_dir = sys.argv[2]
current_dir = os.getcwd()
if not os.path.exists(out_dir):
    os.mkdir(out_dir)
filenames = os.listdir(in_dir)
for filename in filenames:
    f = open (in_dir + "/" + filename , "r")
    f2 = open (out_dir + "/" + filename, "w")
    first_word_in_sent = True
    for line in f:
        if line:
            (word, tag) = line.strip().split()
    	    PP = ""
	    if first_word_in_sent and word[0].isupper():
		word = word.lower()
            if tag[0] == "N" and word in lemma_dictionary_N:
                word = lemma_dictionary_N[word]
            elif tag[0] == "V":
		if word not in lemma_dictionary_V and len(word) > 3:
			if word[-2:] in ["me", "te", "se", "lo", "la", "le", "os"]:
				PP = word [-2:]
				word = word[:-2]
			elif word[-3] in ["nos", "los", "las", "les"]:
				PP = word [-3:]
				word = word[:-3] 
		if word in lemma_dictionary_V:
               	     tag += lemma_dictionary_V[word][1]
		     word = lemma_dictionary_V[word][0]
            elif tag[0] == "A" and word in lemma_dictionary_A:
                word = lemma_dictionary_A[word]
            f2.write(word + "/" + tag + " ")
	    if PP:
		f2.write(PP + "/" + "PP" + " ")
            if word in ["?", ".", "!"]:
                f2.write("\n")
	    if word in ["?", ".", "!"]:
		first_word_in_sent = True
	    else:
		first_word_in_sent = False

f.close()
f2.close()
        
