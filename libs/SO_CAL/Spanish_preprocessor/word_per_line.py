# coding: utf-8
import sys

punct = [chr(133), chr(161), ".", "!", "\"", ",", chr(191), "?", ":", ";","'", "(", ")", "[", "]", "*", "-", chr(148), chr(147), chr(180)]

def split_by_punct(word):
    parts = []
    i = 0
    k = 0
    while i < len(word):
        j = i + 1
        if word[i] in punct:
            while j < len(word) and word[j] == word[i]:
                j += 1 # remove duplicates
            parts.append(word[k:i])
            parts.append(word[i])
            k = j
        i = j
    if k < len(word):
        parts.append(word[k:])
    return parts

f = open(sys.argv[1], "r")
f2 = open(sys.argv[2], "w")
for line in f:
    words = line.strip().split()
    for word in words:
        parts = split_by_punct(word)
        for part in parts:
            if part:
		if part == chr(148) or part == chr(147):
		    part = '"'
		if part == chr(180):
		    part = "'"
                f2.write(part + "\n")
f.close()
f2.close()

            
    
