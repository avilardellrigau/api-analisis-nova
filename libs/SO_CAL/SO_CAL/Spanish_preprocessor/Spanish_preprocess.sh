#!bin/sh

#############################################################################
#
# Spanish_preprocess.sh 
# by Julian Brooke, last modified May 2009
# 
# This program will handle the pre-processing steps involved in setting
# up a directory of Spanish text files for run through SO-CAL
# 
# It prepares the files for the SVMTool, runs the tagger, and then 
# carries out lemmatization
#
# Usage:  sh Spanish_preprocess.sh input_directory output_directory
#
#############################################################################

# Check for correct args:
if [ "$#" -ne 2 ]; then
        echo 1>$2 Usage: $0 INPUT_DIR OUTPUT_DIR
        exit 127 
fi

CURRENT_DIR=`pwd`;

# If the output directory doesn't exist, create it
if [ ! -d "$2" ];
then
        mkdir $2
fi

mkdir $2/temp

# Switch to input dir:
cd $1


for filename in `ls *.txt`
do
	python $CURRENT_DIR/word_per_line.py $CURRENT_DIR/$1/$filename $CURRENT_DIR/$2/tempfile
	cd $CURRENT_DIR/SVMTool-1.3/bin
        perl -I $CURRENT_DIR/SVMTool-1.3/lib SVMTagger $CURRENT_DIR/spa/3LB.SPA < $CURRENT_DIR/$2/tempfile > $CURRENT_DIR/$2/temp/$filename
	cd $CURRENT_DIR
	rm $CURRENT_DIR/$2/tempfile
      cd $CURRENT_DIR
done

python spanish_lemma.py $CURRENT_DIR/$2/temp $CURRENT_DIR/$2
rm -r $CURRENT_DIR/$2/temp
