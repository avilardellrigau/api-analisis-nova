### SO_Mrun.py by Julian Brooke (May 2009)
### see README for more information
import sys
import os

cutoff = ""
config = ""

if sys.argv[1][0] == "-": # There are options
    i = 1
    options = sys.argv[1]
else:
    i = 0
    options = ""

if "s" in options: # set the cutoff
    i += 1
    cutoff = sys.argv[i]
if "c" in options: # special config file
    i += 1
    config = (sys.argv[i])
                   
input_dir = sys.argv[i + 1]
print "Searching " + input_dir
output_dir = sys.argv[i + 2]

current_dir = os.getcwd()
if not os.path.exists (current_dir + "/" + output_dir):
    os.mkdir(output_dir)

in_dirs = os.listdir(input_dir)

p_gt = 0
p_gc = 0
n_gt = 0
n_gc = 0
f = open(output_dir + "/overall.txt", "w")
f2 = open(output_dir + "/finalout.txt", "w")
for in_dir in in_dirs:
    if options:
        new_options = options + "f " + cutoff + " " + config + " "
    else:
        new_options = "-f "
    cmd = "python SO_Run.py " + new_options + input_dir + "/" + in_dir + " " + output_dir + "/" + in_dir
    os.system(cmd)
    infile = open(output_dir + "/" + in_dir + "/overall.txt")
    results = infile.readline()
    infile.close()
    (p_total, p_correct, n_total, n_correct) = map(int, results.split("\t"))
    f.write(in_dir + "\t" + results)
    f2.write ("------\nResults for " + in_dir + ":\n------\n")
    if p_total > 0:
        f2.write (str(p_total) + " Positive Reviews\n")
        f2.write ("Percent Correct: " + str(100*p_correct/float(p_total)) + " %\n")
    if n_total > 0:
        f2.write (str(n_total) + " Negative Reviews\n")
        f2.write ("Percent Correct: " + str(100*n_correct/float(n_total)) + " %\n")
    total = p_total + n_total
    correct = p_correct + n_correct
    if total > 0:
        f2.write (str(total) + " Reviews\n")
        f2.write ("Percent Correct: " + str(100*correct/float(total)) + " %\n")
    p_gt += p_total
    p_gc += p_correct
    n_gt += n_total
    n_gc += n_correct
f2.write ("-----------\nResults for all contents of " + input_dir + ":\n-----------\n")
if p_gt > 0:
    f2.write (str(p_gt) + " Positive Reviews\n")
    f2.write ("Percent Correct: " + str(100*p_gc/float(p_gt)) + " %\n")
if n_gt > 0:
    f2.write (str(n_gt) + " Negative Reviews\n")
    f2.write ("Percent Correct: " + str(100*n_gc/float(n_gt)) + " %\n")
total = p_gt + n_gt
correct = p_gc + n_gc
if total > 0:
    f2.write (str(total) + " Reviews\n")
    f2.write ("Percent Correct: " + str(100*correct/float(total)) + " %\n")
f.close()
f2.close()

if "t" in options: # output stats file
    os.system("python mget_stats.py " + output_dir)
        



