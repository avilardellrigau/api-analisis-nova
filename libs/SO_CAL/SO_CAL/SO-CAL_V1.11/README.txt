SO Calculator
================

Version 1.11 (June, 2009)
-------------------------
SO Calculator is an application for calculating the Semantic Orientation of text documents. It was designed with the domain of online product reviews in mind.  It includes both English and Spanish versions.

License
-------
SO-CAL (Semantic Orientation CALculator)
Copyright (C) 2009 Maite Taboada, Julian Brooke, Kimberly Voll, Caroline
Anthony and Jack Grieve

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Preprocessing
-------------

To preprocess a directory of English textfiles, use

sh preprocess.sh input_directory output_directory

Note that the standard preprocessing makes use of the Brill tagger (Brill 1992), which is included in the zip file. The tagger can be compiled from its main directory using the  make command; you might need to change the compiler in the makefile (e.g. cc) depending what's installed on your system. Once the tagger is compiled, you will probably also need to set permissions on the executables:

chmod +x ~/SO_CAL_V1.1/RULE_BASED TAGGER_V1.14/Bin_and_Data/*

and put the /RULE_BASED_TAGGER_V1.14/Bin_and_Data directory in your path:

setenv PATH ${PATH}:~/SO_CAL_V1.1/RULE_BASED_TAGGER_V1.14/Bin_and_Data

The preprocessing script is written in PERL.

For Spanish preprocessing (which is provided separately), the command is:

sh Spanish_preprocess.sh input_directory output_directory

The spanish preprocessor uses the SVMTool Spanish tagger (Gim�nez and M�rquez, 2004), and makes use of a 500,000+ word lemma dictionary included in the Freeling software package. The output is lemmatized, but preserves the tags.

The Spanish SO-CAL expects Latin-1 input (ISO-8859-1). If you have anything else above that or ASCII (e.g., UTF-8), then you'll have to convert your input to Latin-1.

Other taggers which use the same tagsets may be substituted

Usage
-----

python SO_Run.py [-ynfsct] [cutoff] [config_file] input_directory output_directory

runs a directory of preprocessed texts through the calculator. -y and -n are used to indicate that all the files in the input directory are positive (y) or negative (n); otherwise the program will decide if a text is intended to be positive or negative based on the filename. -f outputs the sums to a file inside of printing output to the screen. -s allows you to set a cutoff between positive and negative reviews other than 0, -c allows you to use an alternate configuration file (the default is SO_Calc.ini). -t will include a stats.txt file in the output directory, providing information about the appearance of 
various words and features. 

python SO_Mrun.py [-sct] [cutoff] [config_file] input_directory output_directory

runs multiple sub-directories of preprocessed text through the calculator. The structure of the input_directory will be mirrored in the output_directory. Otherwise the same as SO_Run

python SO_group.py output_directory -# cutoff1 cutoff2 ... g1_dir g2_dir g3_dir ...

tests a corpus which is more fine-grained than positive and negative. The number of buckets N is given as the second argument (e.g. -4), followed by N - 1 cutoff values, ordered from negative to positive (e.g. -.5 0 5), and then by N directories, each containing only texts that belong in a particular group, i.e. g1_dir would contain only texts which should be classified as group 1 (i.e. a value less than cutoff1 would yield a correct prediction).

Note that you must have Python (ver 2.4-2.5, later versions will require conversion, see www.python.org) and the directory containing the python executible must be in your path. If you are getting errors about being unable to opening dictionaries, make sure the the dic_dir variable in the SO_Calc.ini file is set to the correct location of the dictionaries.

If given an existing output directory, that directory will be overwritten.

To run SO-CAL in Spanish mode, use the included Spa_SO_Calc.ini file (run the program with the -c Spa_SO_Calc.ini option)

I/O
----

The files in the input directory (or directories) should be preprocessed, i.e. tokened and tagged with one sentence on each line. Except when the directories are homogeneous (i.e. with SO_group or when -y or -n options is used), the filenames of the texts should indicate whether they are positive or negative: by default, filenames beginning with "y" will be treated as positive, finenames beginning with "n" as negative (all other files will be ignored). The particular characters which identify positive and negative reviews can be edited in SO_Run.py (pos_mark and neg_mark).

The SO_Calc.ini file contains the various options for SO-Cal, including which dictionaries to use, which features are enabled, etc.; see the comments in SO_Calc.ini or SO_Calc.py for more details about individual options. The last variable in SO_Calc.ini is the list of weight tags; the input files may contain simple XML tags (e.g. <tag> </tag>) which allow an external module to give weights to spans of the text in SO-Cal -- any SO-carrying text appearing between two XML tags will be given the weight specified in the .ini file.

There are four output files in the output directory of SO_Run.py or any particular subdirectory of the output directory for SO_Mrun.py, two of which are optional. Output.txt is a list of all the files and their calculated SO values, one pair on each line, separated by a tab. Richout.txt shows the calculation for each SO-carrying word in each file, including relevant modifiers. If a search for unknown words, used words, or sentences containing particular words is carried out (done by modifying the relevant flags in SO_Calc.ini), the results will appear in search.txt. If the -f command is used, the output directory will also contain an "overall.txt" which contains:
(total number of positive texts) (num of correct positives) (total number of negative texts) (num of correct negatives)
Since SO_Mrun calls SO_Run with the -f flag, all sub- directories of an SO_Mrun output directory will have an overall.txt. After SO_Mrun is called, the main output directory will contain two files: overall.txt and finalout.txt. overall.txt is a concatenation of the overall.txt from each of the subdirectories, and is excel friendly. Finalout gives the statistical results in a somewhat prettier form, with positive, negative, and total correct for each subdirectory as well as the entire input directory.

SO_group.py has a single main output file, finalout.txt, which give the numbers that were classified in each group for each group, and both the overall correct as well as the overall "close" (within 1 bucket of correct).

If you want additional statistics after running SO calculator (for instance, how often each feature was used), use the get_stats.py and mget_stats.py scripts on the output. The get_stats.py takes a single rich output file, and outputs a file of statistics, while the mget_stats.py script just takes a directory with multiple output sub-directories, and creates a stats.py 
