#!bin/sh

#############################################################################
#
# preprocess.sh 
# May 27th, 2008
# by Kimberly Voll
# 
# This program will handle the pre-processing steps involved in setting
# up a text file for run through SO-CALC.  This involves correcting any 
# spacing difficulties via add_space, correcting any negation (contractions)
# using swapNOT, and splitting up text files so that they contain
# one sentence per line, using sentence_boundary, running it through
# the tagger, and finally fixing a common error made by the Brill tagger 
#
# Usage:  sh preprocess.sh input_directory output_directory
#
#############################################################################

# Check for correct args:
if [ $#argv -ne 2 ]; then
        echo 1>$2 Usage: $0 INPUT_DIR OUTPUT_DIR
        exit 127 
fi

CURRENT_DIR=`pwd`;

# If the output directory doesn't exist, create it
if [ ! -d "$2" ];
then
        mkdir $2
fi

# Switch to input dir:
cd $1


for filename in `ls *.txt`
do
	perl $CURRENT_DIR/swapNOT.pl <$filename |
	perl $CURRENT_DIR/sentence_boundary.pl | 
	perl $CURRENT_DIR/add_space.pl > $CURRENT_DIR/$2/tempfile
	cd $CURRENT_DIR/RULE_BASED_TAGGER_V1.14/Bin_and_Data
        ./tagger LEXICON.BROWN $CURRENT_DIR/$2/tempfile BIGRAMS LEXICALRULEFILE.BROWN CONTEXTUALRULEFILE.BROWN > $CURRENT_DIR/$2/tempfile2
	cd $CURRENT_DIR
	perl extract_vbg_ver2.p $CURRENT_DIR/$2/tempfile2 $CURRENT_DIR/$2/$filename
	rm $CURRENT_DIR/$2/tempfile
	rm $CURRENT_DIR/$2/tempfile2
        cd $CURRENT_DIR/$1
done
