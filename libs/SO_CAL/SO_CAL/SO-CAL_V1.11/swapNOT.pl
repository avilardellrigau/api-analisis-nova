# by Caroline Anthony
# July 4, 2005
# This program swaps words ending with n't with their appropriate not equivalnet
#
# modified September 2007 by Julian Brooke
# added contractions with the apostrophe missing
#
# modified November 2007 by Julian Brooke
# now converts special MS apostrophes and quotes to standard ASCII

use strict;

my %swap = ("aren't", "are not", "can't", "can not", "couldn't", "could not", "didn't", "did not", "doesn't", "does not", "don't", "do not", "hadn't", "had not", "hasn't", "has not", "haven't", "have not", "isn't", "is not", "mightn't", "might not", "mustn't", "must not", "oughtn't", "ought not", "shan't", "shall not", "shouldn't", "should not", "wasn't", "was not", "weren't", "were not", "won't", "will not", "wouldn't", "would not", "arent", "are not", "cant", "can not", "couldnt", "could not", "didnt", "did not", "doesnt", "does not", "dont", "do not","hadnt", "had not", "hasnt", "has not", "havent", "have not", "isnt","is not", "mightnt", "might not", "mustnt", "must not", "oughtnt","ought not", "shant", "shall not", "shouldnt", "should not", "wasnt","was not", "werent", "were not", "wont", "will not", "wouldnt", "would not");
   my $line;
   while ($line = <>)
   {
	chomp($line);
        my $badq1 = chr(147);
        my $badq2 = chr(148);
        my $bada = chr(146);
        $line =~ s/$badq1/"/g;  # switch weird quotes for normal ones
        $line =~ s/$badq2/"/g;
        $line =~ s/$bada/'/g;   # switch weird apostrophes for nomal ones
	my @words = split(" ", $line);
	foreach my $word (@words)
	{
		if (exists $swap{$word})
		{
			$word = $swap{$word};
		}
	print "$word ";
	}
   print "\n";
   }
