#!/usr/bin/perl
#
# by Katia Dilkina
# March 23, 2003
#
# separates out all non-word characters from neighbouring characters
# with spaces
#
# modified by Caroline Anthony
# June 9, 2005
# remove duplicate punctuation
#
# modified by Caroline Anthony
# June 28, 2005
# - swap slashes with commas
# - space out words joined by commas
#
# modified by Caroline Anthony
# January 13, 2006
# - include a space between a word and 's / 've etc

use strict;

   my $line;
   # while there are more lines in that file, do the following
   while ($line = <>) {

    chomp($line);

    # swap slashes for commas
    $line =~ s/\//\,/g;

    # look for two words seperated by a comma or slash and space the comma slash
    # note that we are specifically ignoring numbers like $100,000
    while ($line =~ m/([a-z|A-Z]+)([,\/\.\?\!\"\$\%\:\;\]\[\(\)\{\}\*\&\^\#\@\~])([a-z|A-Z]+)/)
    {
    	$line =~ s/([a-z|A-Z]+)([,\/\.\?\!\"\$\%\:\;\]\[\(\)\{\}\*\&\^\#\@\~])([a-z|A-Z]+)/$1$2 $3/g;
    }

    # look for two non-word characters one after the other followed by
    # a word; and space them out from each other and from the word
    # with spaces
    #
    # this will take care of punctuation marks at the beginning of a
    # word

    $line =~ s/(\W)(\W)(\w)/$1 $2 $3/g;

    # also consider words at the beginning of a line

    $line =~ s/^(\W)(\w)/$1 $2/g;

    # look also for a word followed by two non-word characters;
    # and space them out from the word with spaces
    #
    # this will take care of punctuation marks at the end of a word

    $line =~ s/(\w)(\W)(\W)/$1 $2 $3/g;

    # also consider words at the end of a line
    $line =~ s/(\w)(\W)$/$1 $2/g;

    # remove groups of redundant punctuation
    
    $line =~ s/([\,\.\:\;\(\)\"\?\!\'\[\]\{\}\@\#\$\%\^\&\*\=\+\|\\\<\>\/\~\`\-])([\,\.\:\;\(\)\"\?\!\'\[\]\{\}\@\#\$\%\^\&\*\=\+\|\\\<\>\/\~\`\-])*/$1/g;

    # add a space between a word and 's
    
    $line =~ s/(\w)(\')/$1 $2/g;

    # output the line
    print "$line\n";
   }
