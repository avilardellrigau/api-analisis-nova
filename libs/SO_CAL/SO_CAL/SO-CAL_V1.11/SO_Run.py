### SO_Run.py by Julian Brooke (May 2009)
### see README for more information

import sys
import os

cutoff = 0.0
config = "SO_Calc.ini"
pos_mark = "y"
neg_mark = "n"

if sys.argv[1][0] == "-": # there are options
    i = 1
    options = sys.argv[1]
else:
    i = 0
    options = ""

if "s" in options: # set the cutoff
    i += 1
    cutoff = float(sys.argv[i])
if "c" in options: # special config file
    i += 1
    config = (sys.argv[i])
                   
input_dir = sys.argv[i + 1]
print "Searching " + input_dir
output_dir = sys.argv[i + 2]

current_dir = os.getcwd()
if not os.path.exists (current_dir + "/" + output_dir):
    os.mkdir(output_dir)

in_files = os.listdir(input_dir)
output = output_dir + "/output.txt"
if os.path.exists (output):
    os.remove(output)
if os.path.exists (output_dir + "/richout.txt"):
    os.remove(output_dir + "/richout.txt")
if os.path.exists (output_dir + "/search.txt"):
    os.remove(output_dir + "/search.txt")
f = open(output, "w")
f.close()
for filename in in_files:
    print "Processing " + filename
    cmd = "python SO_Calc.py " +config + " " + input_dir + "/" + filename + " " + output_dir + "/output.txt " + output_dir + "/richout.txt "+ output_dir + "/" + "search.txt"
    os.system(cmd)

f = open(output, "r")
p_correct = 0
p_total = 0
n_correct = 0
n_total = 0
for line in f.read().split("\n"):
    if line:
        (filename, SO) = line.split("\t")
        if filename[0] == pos_mark or "y" in options:
            p_total += 1
            if float(SO) > cutoff:
                p_correct += 1
        elif filename[0] == neg_mark or "n" in options:
            n_total += 1
            if float(SO) < cutoff:
                n_correct += 1
f.close()
if not "f" in options:
    print "------\nResults:\n------"
    if p_total > 0:
        print str(p_total) + " Positive Reviews"
        print "Percent Correct: " + str(100.0*p_correct/p_total) + " %"
    if n_total > 0:
        print str(n_total) + " Negative Reviews"
        print "Percent Correct: " + str(100.0*n_correct/n_total) + " %"
    total = p_total + n_total
    correct = p_correct + n_correct
    if not options and total > 0:
        print str(total) + " Reviews"
        print "Percent Correct: " + str(100.0*correct/total) + " %"       
else:
    f = open(output_dir + "/overall.txt", "w")
    f.write(str(p_total) + "\t" + str(p_correct) + "\t" + str(n_total) + "\t" + str(n_correct) + "\n")
    f.close()

if "t" in options: # output stats file
    os.system("python get_stats.py " + output_dir + "/richout.txt " + output_dir + "/stats.txt")




    

